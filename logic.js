function generateBoard(m,n) {
    // REVIEW generate a m*n null matrix. Shuffle function will generate an non-null resolvable grid.

    if (m=="" || m<=0 || n=="" || n<=0 || isNaN(m)|| isNaN(n)) { m = 5; n=7 } // default values if entries are incorrects
    m = parseInt(m); n = parseInt(n)
    if (m > n) { let save = m; m=n; n=save }


    let board = []
        for(let k=0; k<m; k++) {
            board.push(new Array(n).fill(0))
        }
    return shuffle(board, 2*(m+n)+1)
}

function shuffle(board, shuffles) {
    // REVIEW Shuffle the resolvable board by applying n random moves on it

    do {
        for(let k=1; k<=shuffles; k++) {
            let i = Math.floor(Math.random() * board.length),
                j = Math.floor(Math.random() * board[0].length)
            board = changeState(board,i,j)
        }
    } while (isWin(board))
    return board
}

function changeState(board,i,j) {
    // REVIEW Change the state of the played light & adjacent lights (if they exists)

    board[i][j] = (board[i][j]+1) % 2
        if (board[i+1] !== undefined) board[i+1][j] = (board[i+1][j]+1) % 2
        if (board[i-1] !== undefined) board[i-1][j] = (board[i-1][j]+1) % 2
        if (board[i][j+1] !== undefined) board[i][j+1] = (board[i][j+1]+1) % 2
        if (board[i][j-1] !== undefined) board[i][j-1] = (board[i][j-1]+1) % 2
    return board
}

function isWin(board) {
    // REVIEW check if the game is win by checking if line contain a "1"

    return board.every(line => !line.includes(1))
}

function resolve(board) {
    // TODO resolve function
    // return board and array that contains all the movements to resolve 
}
