let board = document.getElementById("board")

let gridSettings = document.getElementById("gridSettings"),
    dimH = document.getElementById("dimH"),
    dimV = document.getElementById("dimV")

let attemptsLine = document.getElementById("attemptsLine"),
    attemptsCount = document.getElementById("attempts")



let matrix,
    attempts = 0

function display(matrix) {
    board.innerHTML = ""
    attemptsCount.innerHTML = attempts
    for(let i=0; i<matrix.length; i++) {
        let row = document.createElement('div')
            row.className = 'row'
        for(let j=0; j<matrix[0].length; j++) {
            let light = document.createElement('div')
            matrix[i][j] == 1 ? light.className = 'lightOn' : light.className = 'lightOff'
            light.setAttribute("onclick", "play(" + i.toString() + "," +j.toString()+ ")")
            row.appendChild(light)
        }
        board.appendChild(row)
    }
}

function play(coordx, coordy) {
    if(!isWin(matrix)) {
        let i = parseInt(coordx),
            j = parseInt(coordy)

        matrix = changeState(matrix, i, j)
        attempts++

        display(matrix)

        if (isWin(matrix)) { 
            board.innerHTML = '<p class="alert"><b class="gold normal">Nice !</b> <span class="normal">👑</span><br>You <b class="gold">won</b> the game.</p>'
        }
    }
    else {
        alert("You already win this game ! Try an other game.")
    }
}

gridSettings.addEventListener('submit', e => {
    e.preventDefault()

    matrix = generateBoard(dimV.value, dimH.value)

    document.getElementById("startButton").innerHTML = "Start new game !"

    attempts = 0
    attemptsLine.style.display = "block"
    display(matrix)
})
